#include "mainwindow.h"
#include "ui_mainwindow.h"



#include <Component/sensor.h>
#include <rectmodel.h>
#include <rectview.h>

#include <tablemodel.h>
#include <tableview.h>
#include <hbox.h>
#include <doublerectview.h>
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    settings = new SettingsDialog;

    manager = sensorList::instance();


    for(int i=0;i<100;i++){
        manager->makeDoubleSensor();
    }

    reader = new ReadWorker(manager->getLinearList());
    model = new TableModel(manager->getLinearList());

    con_0 = new controller_0( page0 , *reader);
    con_1 = new controller_1( page1 , *reader);

    connect(this, SIGNAL(change_port(bool)), con_0, SLOT(set_port_status(bool)));
    connect(this, SIGNAL(change_port(bool)), con_1, SLOT(set_port_status(bool)));

    connect(con_1 , SIGNAL(change_status(bool)) , con_0 , SLOT(set_Task_Status(bool)));
    connect(con_0 , SIGNAL(change_status(bool)) , con_1 , SLOT(set_Task_Status(bool)));

    ui->tabWidget->addTab(&page0 , "array");
    ui->tabWidget->addTab(&page1 , "view");

    for(int i=0;i<manager->getLinearList().size();i++){
        manager->getLinearList().at(i)->init();
    }

    connect(ui->actionSettings , SIGNAL(triggered(bool)) , settings , SLOT(show()));
    connect(ui->actionConnect , SIGNAL(triggered(bool)) , this , SLOT(openSerialPort()));
    connect(ui->actionDisconnect , SIGNAL(triggered(bool)) , this , SLOT(closeSerialPort()));


}
void MainWindow::openSerialPort()
{
    SettingsDialog::Settings openport = settings->settings();

    if( reader->Open(openport)){

        ui->actionConnect->setEnabled(false);
        ui->actionDisconnect->setEnabled(true);
        ui->actionSettings->setEnabled(false);

        reader->StartRead();
        isOpen = true;

    } else {

        isOpen = false;
        QMessageBox::critical(this,"serialport","serialport is not opened");
    }

    Q_EMIT change_port(isOpen);

}

void MainWindow::closeSerialPort()
{
    reader->StopRead();
    reader->Close();

    for(int i=0;i<senslist.size();i++){

        senslist[i]->set_Status(STATUS_INIT);
        senslist[i]->set_B_raw(0);
        senslist[i]->set_T_raw(0);
        senslist[i]->set_Id(0);
        senslist[i]->set_I2c_addr(0);

    }

    ui->actionConnect->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);
    ui->actionSettings->setEnabled(true);
    isOpen = false;

    Q_EMIT change_port(isOpen);

}

MainWindow::~MainWindow()
{
    delete ui;
}
