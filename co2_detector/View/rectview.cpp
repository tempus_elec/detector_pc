#include "rectview.h"
#include <QPainter>
#include <QRect>
#include <QPaintEvent>

RectView::RectView(QWidget *parent) : QWidget(parent)
{

}
RectView::RectView(const RectModel* model): model(model)
{
    connect( model , static_cast< void (RectModel::*)(void) >(&RectModel::updated), this , [=]{ update(); });
}


void RectView::paintEvent(QPaintEvent* event)
{
    QWidget::paintEvent(event);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    QRect r = event->rect();

    painter.setPen(model->getPen());
    painter.setBrush(model->getColor());

    painter.drawRoundedRect(r , 10 ,10);
    painter.save();
    painter.setPen(Qt::black);
    painter.drawText(r, Qt::AlignCenter , model->getName());
    painter.restore();
}
