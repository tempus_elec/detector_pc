#include "doublerectview.h"

DoubleRectView::DoubleRectView(QWidget *parent) : QWidget(parent)
{
    this->setLayout(&layout);
    layout.setSpacing(3);
}
DoubleRectView::DoubleRectView(const DoubleRectModel *model)
{
    rect0 = new RectView(model->getRectModel0());
    rect1 = new RectView(model->getRectModel1());

    this->setLayout(&layout);
    layout.addWidget(rect0);
    layout.addWidget(rect1);

    layout.setContentsMargins(0,0,0,0);
    layout.setSpacing(5);

}

DoubleRectView::DoubleRectView(RectView* rect0 , RectView* rect1)
{
    this->setLayout(&layout);
    layout.addWidget(rect0);
    layout.addWidget(rect1);

    layout.setContentsMargins(0,0,0,0);
    layout.setSpacing(5);
}
void DoubleRectView::addRect(RectView* rect0 , RectView* rect1)
{
    this->rect0 = rect0;
    this->rect1 = rect1;
    layout.addWidget(rect0);
    layout.addWidget(rect1);
}
