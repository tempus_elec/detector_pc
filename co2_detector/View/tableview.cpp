#include "tableview.h"

#include <QDebug>

TableView::TableView() : QTableView(nullptr)
{

}
void TableView::paintEvent(QPaintEvent *e)
{
    QTableView::paintEvent(e);

    if(list.isEmpty()){

        return;
    }


    Q_FOREACH(const QModelIndex& iterator , list){

        QRect rect = visualRect(iterator);
        rect.adjust(2, 2, -2, -2);

        QPen pen(Qt::black, 4);
        QPainter painter(viewport());

        painter.setPen(pen);
        painter.drawRect(rect);
     }
}

void TableView::setModel(QAbstractItemModel* model)
{
    QTableView::setModel(model);

    TableModel *sensorTable = (TableModel*)(model);

    connect(sensorTable,SIGNAL(set_border(QModelIndex,bool)), this , SLOT(setBorder(QModelIndex,bool)));

}
void TableView::setBorder(QModelIndex index,bool flag)
{
    if(flag){

        list.append(index);

    } else {

        if(!list.isEmpty())
            list.removeOne(index);
    }
    update(index);
    //qDebug()<<index.row()<<index.column();

}
