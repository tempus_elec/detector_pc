#ifndef DOUBLERECTVIEW_H
#define DOUBLERECTVIEW_H

#include <QObject>
#include <QWidget>
#include <QHBoxLayout>
#include <rectview.h>
#include <doublerectmodel.h>

class DoubleRectView : public QWidget
{
    Q_OBJECT
public:
    explicit DoubleRectView(QWidget *parent = nullptr);
    DoubleRectView(const DoubleRectModel *model);
    DoubleRectView(RectView* rect0 , RectView* rect1);
    void addRect(RectView* rect0 , RectView* rect1);

    QHBoxLayout layout;

private:
    RectView *rect0;
    RectView *rect1;

signals:

public slots:
};

#endif // DOUBLERECTVIEW_H
