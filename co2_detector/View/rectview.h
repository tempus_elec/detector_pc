#ifndef RECTVIEW_H
#define RECTVIEW_H

#include <QWidget>
#include <rectmodel.h>



class RectView : public QWidget
{
    Q_OBJECT
public:
    explicit RectView(QWidget *parent = nullptr);
    RectView(const RectModel* model);



protected:
    void paintEvent(QPaintEvent* event) override;

private:
    const RectModel* model;


};

#endif // RECTVIEW_H
