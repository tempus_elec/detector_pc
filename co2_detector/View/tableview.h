#ifndef TABLEVIEW_H
#define TABLEVIEW_H

#include <QObject>
#include <QTableView>
#include <QPainter>

#include <tablemodel.h>

class TableView : public QTableView
{
    Q_OBJECT
public:
    TableView();
    void paintEvent(QPaintEvent *e) override;
    void setModel(QAbstractItemModel* model) override;

    QList<QModelIndex> list;

public Q_SLOTS:
    void setBorder(QModelIndex index,bool flag);

};

#endif // TABLEVIEW_H
