#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QTimer>
#include <QTimerEvent>
#include <QQueue>
#include <QSerialPort>
#include <settingsdialog.h>

#include <QMutex>
#include <QList>
#include <sensor.h>
#include <sensorlist.h>

#ifndef CONSTANT_H
    #include <constant.h>
#endif

/*
//#define zmdi_setting                0x10
#define read_Sensor_filter          0x20
#define read_Sensor_No_filter       0x30
#define write_id                    0x40
#define i2c_addr_write              0x50
#define i2c_scan                    0x60

//#define ret_zmdi_setting                zmdi_setting + 1
#define ret_read_Sensor_filter          read_Sensor_filter + 1
#define ret_read_Sensor_No_filter       read_Sensor_No_filter + 1
#define ret_write_id                    write_id + 1
#define ret_i2c_addr_write              i2c_addr_write + 1
#define ret_i2c_scan                    i2c_scan + 1

#define MSG_LENGTH   50
*/



class ReadWorker : public QObject
{
    Q_OBJECT
public:
    ReadWorker(QList<sensor*> list);
    void timerEvent(QTimerEvent *event) override;
    bool Open(SettingsDialog::Settings settings);
    void Close();
    void StartRead();
    void StopRead();
    unsigned short getCRC16(QByteArray temp);
    bool isValidIndex(unsigned int index , unsigned int sub_index);
    void setValue(unsigned int index , unsigned int sub_index , sensor sen);
   // void setValue(unsigned int index , unsigned int sub_index , int b_raw , int t_raw);


    //QTimer timer;
    int id = 0;
    QQueue<QByteArray> cmd_queue;
    QByteArray excute_cmd;
    QByteArray received_data;
    QSerialPort* readerport;
    QMutex mutex;
    QList<sensor*> senslist;


    bool isbusy = false;
    bool iswaiting = false;
    bool isOpen = false;
    int timeout = 0;
    
    static const unsigned char zmdi_setting = 0x10;
    static const unsigned char read_Sensor_filter = 0x20;
    static const unsigned char read_Sensor_No_filter = 0x30;
    static const unsigned char write_id = 0x40;
    static const unsigned char i2c_addr_write = 0x50;
    static const unsigned char i2c_scan = 0x60;

    static const unsigned int MSG_LENGTH = 50;


Q_SIGNALS:
    void update(unsigned int index , unsigned int sub_index);
    void measure(unsigned int index , unsigned int sub_index , bool isMeasure);
    void update_adc(int index , int val);
    void busy();
    void error(QString,QString);


public Q_SLOTS:
    void push_array_read_no_filter(unsigned char str_index , unsigned int length);
    void push_array_init(unsigned char str_index , unsigned int length);
    void push_array_write_id(unsigned char str_index , unsigned int length , unsigned short str_id);
    void push_array_write_i2c_addr(unsigned char str_index ,unsigned int length , unsigned char co2_addr , unsigned char ref_addr);
    void push_array_scan(unsigned char str_index , unsigned int length);
    void init_list();
    void clear_queue();
    void received();
};

#endif // WORKER_H

