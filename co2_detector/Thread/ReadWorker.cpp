#include "ReadWorker.h"
#include <QDebug>

ReadWorker::ReadWorker(QList<sensor*> list)
{
    senslist = list;
    readerport = new QSerialPort;
}

unsigned short ReadWorker::getCRC16(QByteArray temp)
{
    unsigned short CRC16 = 0xffff;
    unsigned short POLY = 0xa001;
    int MessageLength = temp.size();
    int shift=0;
    for(int i=MessageLength-1;i>=0;i--){
        unsigned short code = (unsigned short)(0xff & temp[MessageLength-1-i]);
        CRC16 = CRC16 ^ code;
        shift = 0;
        while(shift<=7){
            if(CRC16 & 0x1){
                CRC16 = CRC16>>1;
                CRC16 = CRC16 ^ POLY;

            } else{
                CRC16 = CRC16>>1;

            }
            shift++;
        }

    }

    return CRC16;
}

void ReadWorker::timerEvent(QTimerEvent *event)
{
    unsigned int index;
    unsigned int sub_index;

    if(event->timerId() == id){
        if(iswaiting){

            if(timeout >= 1){

                timeout = 0;

                mutex.lock();
                iswaiting = false;
                mutex.unlock();

                index = excute_cmd[3] & 0xff;
                sub_index = excute_cmd[4] & 0xff;

                sensor rev;
                rev.set_Status(STATUS_INIT);
                setValue( index , sub_index , rev);


                //senslist[index]->setMeasureFlag(sub_index , false);

            }
            else
                timeout++;


        } else {
            if(!cmd_queue.isEmpty()){
                excute_cmd = cmd_queue.dequeue();

                index = excute_cmd[3] & 0xff;
                sub_index = excute_cmd[4] & 0xff;

                Q_EMIT measure(index , sub_index , true);

                //senslist[index]->setMeasureFlag(sub_index ,true);
                readerport->write(excute_cmd);

                mutex.lock();
                iswaiting = true;
                mutex.unlock();

                timeout = 0;
            }
        }


    }
}
void ReadWorker::received()
{
    received_data += readerport->readAll();

    QByteArray response_temp;
    unsigned short cal_crc = 0x0;
    unsigned short rev_crc = 0x0;
    if(received_data.size() >= MSG_LENGTH){

        response_temp = received_data.mid(0, received_data.size() - 2);
        cal_crc = getCRC16(response_temp);
        rev_crc = ToUShort(received_data[received_data.size() - 1] , received_data[received_data.size() - 2]);

        unsigned int index;// = received_data[2] & 0xff;
        unsigned int sub_index ;//= received_data[3] & 0xff;
        unsigned char i2c_addr;
        unsigned int cmd = received_data[2] & 0xff;
        unsigned short zmdi_id;

        int adc[10];

        int b_raw;
        int t_raw;
        unsigned int status;

        if(received_data[0] != 0xbb || received_data[1] != 0x66)
        {
            Q_EMIT error("Thread","sync data is fault");
            received_data.clear();
            return;
        }
        if( cal_crc != rev_crc ){
            Q_EMIT error("Thread","crc is invalid");
            received_data.clear();
            return;
        }



        switch (cmd) {
        case zmdi_setting + 1:
        {
            int offset = 0x3;

            index = received_data[offset++] & 0xff;
            sub_index = received_data[offset++] & 0xff;
            i2c_addr = received_data[offset++] & 0xff;
            zmdi_id = ToUShort( received_data[offset + 1] , received_data[offset]);
            offset+=2;

            if(!isValidIndex(index, sub_index)){
                Q_EMIT error("Thread","index is invalid");
                received_data.clear();
                return;
            }
            status = received_data[offset++] & 0xff;

            sensor rev;
            rev.set_Status(status);
            rev.set_I2c_addr( i2c_addr );
            rev.set_B_raw(0x0);
            rev.set_T_raw(0x0);
            rev.set_Id(zmdi_id);

            setValue(index , sub_index , rev);

            int adc_index = offset;
            for(int i=0;i<10;i++){
                adc[i] =  ToUInt( 0x0 , 0x0 , received_data[adc_index+1] , received_data[adc_index] );
                adc_index += 2;
                update_adc(i , adc[i]);
            }
        }


            break;
        case write_id + 1:
        {
            int offset = 0x3;

            index = received_data[offset++] & 0xff;
            sub_index = received_data[offset++] & 0xff;
            i2c_addr = received_data[offset++] & 0xff;
            zmdi_id = ToUShort( received_data[offset + 1] , received_data[offset]);
            offset += 2;

            if(!isValidIndex(index,sub_index)){
                Q_EMIT error("Thread","index is invalid");
                received_data.clear();
                return;
            }
            status = received_data[offset++] & 0xff;


            sensor rev;
            rev.set_Status(status);
            rev.set_I2c_addr( i2c_addr );
            rev.set_B_raw(0x0);
            rev.set_T_raw(0x0);
            rev.set_Id(zmdi_id);
            setValue(index , sub_index , rev);

        }
            break;

        case read_Sensor_No_filter + 1:
        {
            int offset = 0x3;
            index = received_data[offset++] & 0xff;
            sub_index = received_data[offset++] & 0xff;
            i2c_addr = received_data[offset++] & 0xff;
            zmdi_id = ToUShort( received_data[offset + 1] , received_data[offset]);
            offset += 2;

            if(!isValidIndex(index,sub_index)){
                Q_EMIT error("Thread","index is invalid");
                received_data.clear();
                return;
            }
            status = received_data[offset++] & 0xff;
            b_raw = ToUInt(received_data[offset + 3] , received_data[offset + 2] , received_data[offset + 1] , received_data[offset]);
            offset += 4;
            t_raw = ToUInt(received_data[offset + 3] , received_data[offset + 2] , received_data[offset + 1] , received_data[offset]);
            offset += 4;

            sensor rev;
            rev.set_Status(status);
            rev.set_I2c_addr( i2c_addr );
            rev.set_B_raw(b_raw);
            rev.set_T_raw(t_raw);
            rev.set_Id(zmdi_id);
            setValue(index , sub_index , rev);

        }
            break;
        case i2c_addr_write + 1:
        {
            int offset = 0x3;
            unsigned char rev_i2c = 0x0;
            unsigned char read_mem = 0x0;

            index = received_data[offset++] & 0xff;
            sub_index = received_data[offset++] & 0xff;
            i2c_addr = received_data[offset++] & 0xff;
            zmdi_id = ToUShort( received_data[offset + 1] , received_data[offset]);
            offset+=2;

            if(!isValidIndex(index,sub_index)){
                Q_EMIT error("Thread","index is invalid");
                received_data.clear();
                return;
            }
            status = received_data[offset++] & 0xff;

            sensor rev;
            rev.set_Status(status);
            rev.set_I2c_addr( i2c_addr );
            int adc_index = offset;

            setValue(index , sub_index , rev);


        }
            break;
        case i2c_scan + 1:
        {
            int offset = 0x3;
            unsigned char rev_addr = 0x0;
            index = received_data[offset++] & 0xff;
            sub_index = received_data[offset++] & 0xff;
            i2c_addr = received_data[offset++] & 0xff;
            offset+=2;

            if(!isValidIndex(index,sub_index)){
                Q_EMIT error("Thread","index is invalid");
                received_data.clear();
                return;
            }

            status = received_data[offset++] & 0xff;
            rev_addr = received_data[offset++] & 0xff;

            sensor rev;
            rev.set_Status(status);
            rev.set_I2c_addr(rev_addr);


            setValue(index , sub_index , rev);


        }
            break;

        default:
            break;
        }

        received_data.clear();
        timeout = 0;

        mutex.lock();
        iswaiting = false;
        mutex.unlock();

    }
}
bool ReadWorker::isValidIndex(unsigned int index , unsigned int sub_index)
{
    return (index < (sensorList::instance()->getDoubleList().size()) && sub_index < 0x2) ? true : false;

}

void ReadWorker::setValue(unsigned int index , unsigned int sub_index , sensor sen)
{
    try{
        /*
            get->setMeasureFlag(sub_index , false);
        */
        sensorList *get = sensorList::instance();
        get->set_Status( index, sub_index, sen.get_Status() );
        get->set_B_raw( index , sub_index , sen.get_B_raw() );
        get->set_T_raw( index, sub_index , sen.get_T_raw() );
        get->set_Id( index , sub_index , sen.get_Id() );
        get->set_I2c_addr( index , sub_index , sen.get_I2c_addr() );

        Q_EMIT measure(index , sub_index , false);
        Q_EMIT update(index , sub_index);

    }catch(const char* ex){
        Q_EMIT error("Thread",ex);
    }
}

void ReadWorker::StartRead()
{
    readerport->clear();
    cmd_queue.clear();
    received_data.clear();
    id = startTimer(400);



}
void ReadWorker::StopRead()
{
    if(id != 0){
        killTimer(id);
        id = 0;
    }
}

bool ReadWorker::Open(SettingsDialog::Settings settings)
{
    readerport->setBaudRate(settings.baudRate);
    readerport->setDataBits(settings.dataBits);
    readerport->setPortName(settings.name);
    readerport->setParity(settings.parity);
    readerport->setStopBits(settings.stopBits);
    readerport->setFlowControl(settings.flowControl);
    bool ret = readerport->open(QIODevice::ReadWrite);
    if( ret )
        connect(readerport , SIGNAL(readyRead()),this , SLOT(received()));
    return ret;

}

void ReadWorker::Close()
{
    disconnect(readerport , SIGNAL(readyRead()),this , SLOT(received()));
    readerport->close();

    for(int i=0;i<senslist.size();i++)
        senslist[i]->init();
}

void ReadWorker::push_array_read_no_filter(unsigned char str_index , unsigned int length)
{
    unsigned int limit = str_index + length;

    for(unsigned char i=str_index;i<limit;i++){
        for(unsigned char j=0;j<2;j++){



            QByteArray cmd;
            cmd.append(0xaa);
            cmd.append(0x55);
            cmd.append(read_Sensor_No_filter);
            cmd.append(i);
            cmd.append(j);
            cmd.append(sensorList::instance()->get_I2c_addr(i,j));
            for(int k=0;k<MSG_LENGTH - 8;k++)
                cmd.append((char)0x0);
            unsigned short crc = getCRC16(cmd);
            cmd.append(crc & 0xff);
            cmd.append((crc >> 8) & 0xff);
            cmd_queue.enqueue(cmd);
        }

    }
}


void ReadWorker::push_array_init(unsigned char str_index , unsigned int length)
{
    unsigned int limit = str_index + length;

    for(unsigned char i=str_index;i<limit;i++){
        for(unsigned char j=0;j<2;j++){

            QByteArray cmd;

            cmd.append(0xaa);
            cmd.append(0x55);
            cmd.append(zmdi_setting);
            cmd.append(i);
            cmd.append(j);
            cmd.append(sensorList::instance()->get_I2c_addr(i,j));
            for(int k=0;k<MSG_LENGTH - 8;k++)
                cmd.append((char)0x00);
            unsigned short crc = getCRC16(cmd);
            cmd.append(crc & 0xff);
            cmd.append((crc >> 8) & 0xff);
            cmd_queue.enqueue(cmd);
        }
    }
}
void ReadWorker::push_array_write_id(unsigned char str_index , unsigned int length , unsigned short str_id)
{

    unsigned int limit = str_index + length;


    unsigned short start_id = str_id;

    for(unsigned char i = str_index ; i<limit ; i++){
        for(unsigned char j = 0; j<2;j++){

            unsigned char id_high = ToByte(start_id , 1);
            unsigned char id_low = ToByte(start_id , 0);
            QByteArray cmd;

            cmd.append(0xaa);
            cmd.append(0x55);
            cmd.append(write_id);
            cmd.append(i);
            cmd.append(j);
            cmd.append(sensorList::instance()->get_I2c_addr(i,j));
            cmd.append(id_high);
            cmd.append(id_low);
            for(int k=0;k<MSG_LENGTH - 10;k++)
                cmd.append((char)0x0);
            unsigned short crc = getCRC16(cmd);
            cmd.append(crc & 0xff);
            cmd.append((crc >> 8) & 0xff);
            cmd_queue.enqueue(cmd);
        }
        start_id++;

    }

}
void ReadWorker::push_array_scan(unsigned char str_index , unsigned int length)
{
    unsigned int limit = str_index + length;

    for( unsigned char i = str_index ; i < limit ; i++){
        for(unsigned char j = 0; j<2;j++){

            QByteArray cmd;
            cmd.append(0xaa);
            cmd.append(0x55);
            cmd.append(i2c_scan);
            cmd.append(i);
            cmd.append(j);
            cmd.append(sensorList::instance()->get_I2c_addr(i,j));
            for(int k=0;k<MSG_LENGTH - 8;k++)
                cmd.append((char)0x0);
            unsigned short crc = getCRC16(cmd);
            cmd.append(crc & 0xff);
            cmd.append((crc >> 8) & 0xff);
            cmd_queue.enqueue(cmd);
        }
    }


}
void ReadWorker::push_array_write_i2c_addr(unsigned char str_index ,unsigned int length , unsigned char co2_addr , unsigned char ref_addr)
{

    unsigned int limit = str_index + length;


    for(unsigned char i = str_index ; i < limit ; i++){
        for(unsigned char j = 0; j<2;j++){

            QByteArray cmd;
            cmd.append(0xaa);
            cmd.append(0x55);
            cmd.append(i2c_addr_write);
            cmd.append(i);
            cmd.append(j);
            cmd.append(sensorList::instance()->get_I2c_addr(i,j));
            if( j == 0)
                cmd.append(co2_addr);
            else
                cmd.append(ref_addr);

            for(int k=0;k<MSG_LENGTH - 9;k++)
                cmd.append((char)0x0);
            unsigned short crc = getCRC16(cmd);
            cmd.append(crc & 0xff);
            cmd.append((crc >> 8) & 0xff);

            cmd_queue.enqueue(cmd);


        }




    }

}

void ReadWorker::init_list()
{
    /*
    for(int i=0;i<senslist.size();i++){
        for(int j=0;j<2;j++){
            senslist[i]->setStatus(j , STATUS_INIT);
            senslist[i]->setBraw(j , 0);
            senslist[i]->setTraw(j , 0);
        }
    }
    */
}

void ReadWorker::clear_queue()
{
     cmd_queue.clear();
}
