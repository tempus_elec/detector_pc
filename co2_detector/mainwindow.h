#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <sensor.h>
#include <page_0.h>
#include <page_1.h>
#include <rectmodel.h>
#include <ReadWorker.h>
#include <settingsdialog.h>
#include <controller_0.h>
#include <controller_1.h>
#include <sensorlist.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QList<sensor*> senslist;
    QList< QList<sensor*>> seperate_list;

    page_0 page0;
    page_1 page1;

    controller_0 *con_0;
    controller_1 *con_1;


    TableModel *model;
    SettingsDialog *settings;
    ReadWorker *reader;

    sensorList *manager;

Q_SIGNALS:
   void change_port(bool port_status);

public Q_SLOTS:
   void openSerialPort();
   void closeSerialPort();

private:
    Ui::MainWindow *ui;
    bool isOpen = false;
};

#endif // MAINWINDOW_H
