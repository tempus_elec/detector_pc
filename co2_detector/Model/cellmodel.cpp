#include "cellmodel.h"
#include "tablemodel.h"

#include "sensorlist.h"



const QStringList CellModel::column_header =  { "STATUS" , "B_RAW" , "T_RAW" , "ID" , "I2C_ADDR" };


CellModel::CellModel(sensor &sen , int index) : RectModel(sen)
{
    if(sen.get_Type() == CO2)
        name = "C" + QString::number(sen.get_Index());
    else
        name = "R" + QString::number(sen.get_Index());

    this->index = index;
    type = sen.get_Type();

    connect(&sen , static_cast<void (sensor::*)(int)>(&sensor::changed_B_raw) , this , [=](int b){ b_raw = b; Q_EMIT changed_B_raw( index , B_RAW_COLUMN); });
    connect(&sen , static_cast<void (sensor::*)(int)>(&sensor::changed_T_raw) , this , [=](int t){ t_raw = t; Q_EMIT changed_T_raw( index , T_RAW_COLUMN); });
    connect(&sen , static_cast<void (sensor::*)(unsigned short)>(&sensor::changed_Id) , this , [=](unsigned short id){ zmdi_id = id; Q_EMIT changed_Id( index , ID_COLUMN); });
    connect(&sen , static_cast<void (sensor::*)(unsigned char)>(&sensor::changed_I2c_addr) , this , [=](unsigned char addr){ i2c_addr = addr; Q_EMIT changed_I2c_addr( index , I2C_ADDR_COLUMN); });
}

void CellModel::changed_Color(int status)
{
    switch(status){
    case STATUS_OK:
        color = QColor(Qt::green);
        status_str = "Ok";
        break;
    case STATUS_FAIL:
        color = QColor(Qt::red);
        status_str = "Fail";
        break;
    case STATUS_INIT:
        color = QColor(Qt::gray);
        status_str = "Init";
        break;
    }

    Q_EMIT changed_Status( index , STATUS_COLUMN);

}
void CellModel::setLineVisible(bool isVisible)
{

    Q_EMIT changed_Border( index , STATUS_COLUMN , isVisible);
}

QString CellModel::get_B_raw()
{
    return QString("%1").arg(b_raw);
}

QString CellModel::get_T_raw()
{
    return QString("%1").arg(t_raw);
}

QString CellModel::get_Status()
{
    return status_str;
}

QString CellModel::get_Id()
{
    return QString("%1").arg(zmdi_id);
}

QString CellModel::get_I2c_addr()
{
    return QString("%1").arg(i2c_addr);
}
QColor CellModel::get_StatusColor()
{
    return color;
}
