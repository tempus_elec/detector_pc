#ifndef RECTMODEL_H
#define RECTMODEL_H

#include <QObject>
#include <QPen>
#include <QColor>
#include <QString>
#include <sensor.h>

#ifndef CONSTANT_H
    #include <constant.h>
#endif


class RectModel : public QObject
{
    Q_OBJECT
public:

    RectModel( const sensor& sen);

    QColor getColor() const;
    QPen getPen() const;
    QString getName() const;

Q_SIGNALS:
    void updated();

public Q_SLOTS:
    virtual void changed_Color(int status);
    virtual void setLineVisible(bool isVisible);



protected :
    QColor color;
    QString name;
private:
    QPen pen;

};

#endif // RECTMODEL_H
