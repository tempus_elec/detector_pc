#include "rectmodel.h"



RectModel::RectModel( const sensor& sen)
{
    if(sen.get_Type() == CO2)
        name = "C\n" + QString::number(sen.get_Index());
    else
        name = "R\n" + QString::number(sen.get_Index());

    connect( &sen , SIGNAL(changed_Status(int)) , this , SLOT(changed_Color(int)));

    pen.setWidth(0);
    pen.setColor(QColor(Qt::gray));

}

QColor RectModel::getColor() const
{
    return color;
}
QPen RectModel::getPen() const
{
    return pen;
}
QString RectModel::getName() const
{
    return name;
}
void RectModel::changed_Color(int status)
{
    switch(status){
    case STATUS_OK:
        color = QColor(Qt::green);
        break;
    case STATUS_FAIL:
        color = QColor(Qt::red);
        break;
    case STATUS_INIT:
        color = QColor(Qt::gray);
        break;
    }
    pen.setColor(color);

    Q_EMIT updated();
}
void RectModel::setLineVisible(bool isVisible)
{
    if( isVisible ){
        pen.setColor(QColor(Qt::black));
        pen.setWidth(4);


    } else{
        pen.setColor(color);
        pen.setWidth(0);
    }
    Q_EMIT updated();
}
