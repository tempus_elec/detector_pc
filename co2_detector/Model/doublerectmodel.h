#ifndef DOUBLERECTMODEL_H
#define DOUBLERECTMODEL_H

#include <QObject>
#include <sensor.h>
#include <doublesensor.h>
#include <rectmodel.h>

class DoubleRectModel : public QObject
{
    Q_OBJECT
public:
    DoubleRectModel(const doublesensor* sensor);
    RectModel* getRectModel0() const;
    RectModel* getRectModel1() const;
    void setBorder0(bool flag);
    void setBorder1(bool flag);


private:
    RectModel *rect0;
    RectModel *rect1;

signals:

public slots:
};

#endif // DOUBLERECTMODEL_H
