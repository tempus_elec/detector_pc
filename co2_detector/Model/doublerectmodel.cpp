#include "doublerectmodel.h"


DoubleRectModel::DoubleRectModel(const doublesensor* sensor)
{
    rect0 = new RectModel(*(sensor->getSensor0()));
    rect1 = new RectModel(*(sensor->getSensor1()));

}
RectModel* DoubleRectModel::getRectModel0() const
{
    return rect0;
}
RectModel* DoubleRectModel::getRectModel1() const
{
    return rect1;
}
void DoubleRectModel::setBorder0(bool flag)
{
    rect0->setLineVisible(flag);
}

void DoubleRectModel::setBorder1(bool flag)
{
    rect1->setLineVisible(flag);
}
