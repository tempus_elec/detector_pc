#ifndef CELLMODEL_H
#define CELLMODEL_H

#include <QStringList>
#include <QString>
#include <rectmodel.h>
#include <sensor.h>


class CellModel : public RectModel
{
    Q_OBJECT
public:

    static const int STATUS_COLUMN = 0;
    static const int B_RAW_COLUMN = STATUS_COLUMN + 1;
    static const int T_RAW_COLUMN = B_RAW_COLUMN + 1;
    static const int ID_COLUMN = T_RAW_COLUMN + 1;
    static const int I2C_ADDR_COLUMN = ID_COLUMN + 1;
    static const int COLUMN_COUNT = I2C_ADDR_COLUMN + 1;

    static const QStringList column_header;



    CellModel(sensor &sensor,int index);

    QString get_B_raw();
    QString get_T_raw();
    QString get_Status();
    QString get_Id();
    QString get_I2c_addr();

    QColor get_StatusColor();

Q_SIGNALS:
    void changed_B_raw(int row , int col);
    void changed_T_raw(int row , int col);
    void changed_Status(int row , int col);
    void changed_Id(int row , int col);
    void changed_I2c_addr(int row, int col);
    void changed_Border(int row , int col , bool isVisible);

public Q_SLOTS:
    virtual void changed_Color(int status);
    virtual void setLineVisible(bool isVisible);



private:
    QString status_str;
    int index = 0;
    int type = CO2;

    int b_raw = 0;
    int t_raw = 0;
    int status = 0;

    unsigned short zmdi_id = 0x0;
    unsigned char i2c_addr = 0x0;
};

#endif // CELLMODEL_H
