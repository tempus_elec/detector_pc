#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include <QList>
#include <sensor.h>
#include <cellmodel.h>



class TableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    TableModel(QList<sensor*> list);


    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    void border_visible(unsigned int index, unsigned int sub_index , bool flag);

Q_SIGNALS:
    void set_border(QModelIndex index , bool isVisible);


public Q_SLOTS:
    void update_cell(int row, int col);
    void update_border(int row , int col , bool isVisible);


private:
    QList<CellModel*> tablelist;
};

#endif // TABLEMODEL_H
