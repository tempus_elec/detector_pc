#include "tablemodel.h"
#include <QDebug>

TableModel::TableModel(QList<sensor*> list)
{

    for(int i=0;i<list.size();i++){

        tablelist.append(new CellModel((*list[i]) , i));
        connect(tablelist[i], SIGNAL(changed_B_raw(int,int)) , this , SLOT(update_cell(int,int)));
        connect(tablelist[i], SIGNAL(changed_T_raw(int,int)) , this , SLOT(update_cell(int,int)));
        connect(tablelist[i], SIGNAL(changed_Status(int,int)) , this , SLOT(update_cell(int,int)));
        connect(tablelist[i], SIGNAL(changed_Id(int,int)) , this , SLOT(update_cell(int,int)));
        connect(tablelist[i], SIGNAL(changed_I2c_addr(int,int)) , this , SLOT(update_cell(int,int)));
        connect(tablelist[i], SIGNAL(changed_Border(int,int,bool)) , this , SLOT(update_border(int,int,bool)));

    }
}

int TableModel::rowCount(const QModelIndex &parent /* = QModelIndex()*/) const
{
    return tablelist.size();
}
int TableModel::columnCount(const QModelIndex &parent  /* = QModelIndex()*/) const
{
    return CellModel::COLUMN_COUNT;
}
QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role /* = Qt::DisplayRole */) const
{
    if(role == Qt::DisplayRole)
    {
        if(orientation == Qt::Vertical){
            return tablelist[section]->getName();

        } else{

            if( section < tablelist.size())
                return CellModel::column_header[section];
            else
                return QVariant();
        }
    }

    return QVariant();
}
void TableModel::border_visible(unsigned int index, unsigned int sub_index , bool flag)
{
    int ref = index*2 + sub_index;
    tablelist[ref]->setLineVisible(flag);

}

QVariant TableModel::data(const QModelIndex &index, int role /*= Qt::DisplayRole */) const
{
    int row = index.row();
    int col = index.column();

    if(role == Qt::DisplayRole)
    {
        if(col == CellModel::STATUS_COLUMN)
                return tablelist[row]->get_Status();
        if(col == CellModel::B_RAW_COLUMN)
                return tablelist[row]->get_B_raw();
        if(col == CellModel::T_RAW_COLUMN)
                return tablelist[row]->get_T_raw();
        if(col == CellModel::ID_COLUMN)
                return tablelist[row]->get_Id();
        if(col == CellModel::I2C_ADDR_COLUMN)
                return tablelist[row]->get_I2c_addr();

    }
    if(role == Qt::BackgroundColorRole)
    {
        if(col == CellModel::STATUS_COLUMN){
            return QBrush(tablelist[row]->get_StatusColor());
        }
    }

    return QVariant();
}
void TableModel::update_cell(int row, int col)
{
    QModelIndex point = this->index(row , col);
    Q_EMIT dataChanged(point , point);


}
void TableModel::update_border(int row , int col , bool isVisible)
{

    QModelIndex point = this->index(row , col);
    Q_EMIT set_border(point , isVisible);


}
