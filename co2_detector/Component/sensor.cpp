#include "sensor.h"

sensor::sensor(QObject *parent) : QObject(parent)
{

}
sensor::sensor(int index)
{
    /*
    this->index = index;
    number = index / 2;
    type = index % 2;
    */
    this->index = index;
    type = CO2;
}
sensor::sensor(int index , int type)
{
    this->index = index;
    switch(type){
    case CO2:
    case REF:
        this->type = type;
        break;
    default:
        this->type = CO2;
    }
}

sensor::sensor(const sensor &other)
{
    this->b_raw = other.get_B_raw();
    this->t_raw = other.get_T_raw();
    this->status = other.get_Status();
    this->zmdi_id = other.get_Id();
    this->i2c_addr = other.get_I2c_addr();
    this->index = other.get_Index();
    this->type = other.get_Type();
}
void sensor::init()
{
    set_B_raw(0);
    set_T_raw(0);
    set_Status(STATUS_INIT);
    set_Id(0);
    set_I2c_addr(0);
}

void sensor::set_B_raw(int b)
{
    b_raw = b;
    Q_EMIT changed_B_raw(b_raw);
}

int sensor::get_B_raw() const
{
    return b_raw;
}

void sensor::set_T_raw(int t)
{
    t_raw = t;
    Q_EMIT changed_T_raw(t_raw);
}

int sensor::get_T_raw() const
{
    return t_raw;
}

void sensor::set_Status(int status)
{
    this->status = status;
    Q_EMIT changed_Status(this->status);
}

int sensor::get_Status() const
{
    return status;
}

void sensor::set_Id(unsigned short id)
{
    zmdi_id = id;
    Q_EMIT changed_Id(zmdi_id);
}

unsigned short sensor::get_Id() const
{
    return zmdi_id;
}

void sensor::set_I2c_addr(unsigned char addr)
{
    i2c_addr = addr;
    Q_EMIT changed_I2c_addr(i2c_addr);
}

unsigned char sensor::get_I2c_addr() const
{
    return i2c_addr;
}
int sensor::get_Index() const
{
    return index;
}
int sensor::get_Type() const
{
    return type;
}

sensor& sensor::operator=(const sensor& sen)
{
    sensor temp = sen;
    return temp;
}
