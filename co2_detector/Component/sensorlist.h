#ifndef SENSORLIST_H
#define SENSORLIST_H

#include <QObject>
#include <QList>
#include <doublesensor.h>
#include <QMutex>

class sensorList : public QObject
{
    Q_OBJECT
public:
    static sensorList* instance();
    QList<sensor*> getLinearList();
    QList<doublesensor*> getDoubleList();
    void makeDoubleSensor();

    void set_B_raw(int index , int sub_index , int b);
    void set_T_raw(int index , int sub_index , int t);
    void set_Status(int index , int sub_index , int status);
    void set_Id(int index , int sub_index , unsigned short id);
    void set_I2c_addr(int index , int sub_index , unsigned char addr);

    int get_B_raw(int index , int sub_index );
    int get_T_raw(int index , int sub_index );
    int get_Status(int index , int sub_index );
    unsigned short get_Id(int index , int sub_index );
    unsigned char get_I2c_addr(int index , int sub_index );

    static int get_LinearIndex(int index , int sub_index);

    sensor get_Sensor(int index , int sub_index);


private:
    QList<doublesensor*> doublelist;
    QList<sensor*> linearlist;
    static sensorList* m_Instance;



signals:

public slots:
};

#endif // SENSORLIST_H
