#ifndef SENSOR_H
#define SENSOR_H

#include <QObject>

#ifndef CONSTANT_H
    #include <constant.h>
#endif

class sensor : public QObject
{
    Q_OBJECT
public:

    explicit sensor(QObject *parent = nullptr);
    sensor(int index);
    sensor(int index , int type);
    sensor(const sensor& other);

    void init();

    void set_B_raw(int b);
    int get_B_raw() const;

    void set_T_raw(int t);
    int get_T_raw() const;

    void set_Status(int status);
    int get_Status() const;

    void set_Id(unsigned short id);
    unsigned short get_Id() const;

    void set_I2c_addr(unsigned char addr);
    unsigned char get_I2c_addr() const;

    /*
    void set_Index(int index);
    int get_Index() const;

    void set_Type(int type);
    int get_Type() const;
    */

    int get_Index() const;
    int get_Type() const;


    sensor& operator =(const sensor& sen);


private:
    int b_raw = 0;
    int t_raw = 0;
    int status = 0;

    unsigned short zmdi_id = 0x0;
    unsigned char i2c_addr = 0x0;

    int index = 0;
    int number = 0;
    int type = CO2;


Q_SIGNALS:
    void changed_B_raw(int b);
    void changed_T_raw(int t);
    void changed_Status(int status);
    void changed_Id(unsigned short id);
    void changed_I2c_addr(unsigned char addr);



};

Q_DECLARE_METATYPE(sensor)

#endif // SENSOR_H
