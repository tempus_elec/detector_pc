#ifndef DOUBLESENSOR_H
#define DOUBLESENSOR_H

#include <QObject>
#include <sensor.h>

class doublesensor : public QObject
{
    Q_OBJECT
public:
    doublesensor(int index);
    sensor* getSensor0() const;
    sensor* getSensor1() const;

private:
    sensor* sensor0;
    sensor* sensor1;

signals:

public slots:
};

#endif // DOUBLESENSOR_H
