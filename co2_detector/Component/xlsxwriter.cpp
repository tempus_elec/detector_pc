#include "xlsxwriter.h"

xlsxwriter::xlsxwriter(QObject *parent) : QObject(parent)
{
    min = 0;
    max = 99;
    array.resize(100);
    for(int i=0;i<100;i++)
        array[i].resize(2);

    for(int i=0;i<100;i++)
        for(int j=0;j<2;j++){
            array[i][j].col_base = 1;
            array[i][j].row_base = 1;
        }
}
QString xlsxwriter::getFileName()
{
    return path;
}

void xlsxwriter::setFileName(QString path)
{
    this->path = path;
}

void xlsxwriter::change_min_index(int min)
{
    this->min = min;

}

void xlsxwriter::change_max_index(int max)
{
    this->max = max;

}

bool xlsxwriter::open()
{
    if( path.isEmpty())
        return false;

    io = new Document(path);
    makesheet();
    connect(this , SIGNAL(save_signal_id(unsigned char , unsigned char  , int  , int  , int , unsigned short)) , this , SLOT(saveData_by_id(unsigned char ,unsigned char ,int  ,int  ,int ,unsigned short)));

}

void xlsxwriter::close()
{
    if(!path.isEmpty()){
        io->save();

        disconnect(this , SIGNAL(save_signal_id(unsigned char , unsigned char  , int  , int  , int , unsigned short)) , this , SLOT(saveData_by_id(unsigned char ,unsigned char ,int  ,int  ,int ,unsigned short)));
        delete io;
        io = nullptr;
    } else{
        io = nullptr;
    }

}


void xlsxwriter::makesheet()
{


        io->addSheet(QDateTime::currentDateTime().toString("yyyy_MMM_dd_ddd_hh_mm_ss"));
        int col_num = 1;

        for(int i = min; i < max + 1;i++){
            for(int j=0;j<2;j++){
                array[i][j].row_base = 1;
                if(j == CO2) {
                    col_num++;
                    io->write(array[i][j].row_base,  col_num , "CO2_" + QString::number(i));
                }
                else {
                    io->write(array[i][j].row_base,  col_num , "REF_" + QString::number(i));
                }
                array[i][j].row_base++;
                array[i][j].col_base = col_num;

                io->write(array[i][j].row_base , col_num++, "id");
                io->write(array[i][j].row_base,  col_num++, "status");
                io->write(array[i][j].row_base , col_num++, "braw");
                io->write(array[i][j].row_base , col_num++, "traw");
                array[i][j].row_base++;
                //qDebug()<<i<<j<<array[i][j].row_base<<array[i][j].col_base;
            }
        }


}
void xlsxwriter::saveData_by_id(unsigned char index , unsigned char sub_index , int status , int b_raw  ,int t_raw , unsigned short zmdi_id)
{
    try{
            Format format;
            io->write(array[index][sub_index].row_base , array[index][sub_index].col_base , zmdi_id);
            if(status == STATUS_OK) {
                format.setPatternBackgroundColor(QColor(Qt::green));
                io->write(array[index][sub_index].row_base , array[index][sub_index].col_base +1 , "OK"  , format);
            }
            else if(status == STATUS_FAIL) {
                format.setPatternBackgroundColor(QColor(Qt::red));
                io->write(array[index][sub_index].row_base , array[index][sub_index].col_base +1, "FAIL" , format);
            }
            else {
                format.setPatternBackgroundColor(QColor(Qt::gray));
                io->write(array[index][sub_index].row_base , array[index][sub_index].col_base +1, "INIT" , format);
            }
            io->write(array[index][sub_index].row_base , array[index][sub_index].col_base + 2 , b_raw );
            io->write(array[index][sub_index].row_base , array[index][sub_index].col_base + 3 , t_raw);
            array[index][sub_index].row_base++;



    }catch(...){


    }
}
/*
void xlsxwriter::saveData_by_id(sensor sen)
{
    try{
            Format format;
            io->write(array[sen.get_Number()][sen.get_Type()].row_base , array[sen.get_Number()][sen.get_Type()].col_base , sen.get_Id());
            if(sen.get_Status() == STATUS_OK){
                format.setPatternBackgroundColor(QColor(Qt::green));
                io->write(array[sen.get_Number()][sen.get_Type()].row_base , array[sen.get_Number()][sen.get_Type()].col_base +1 , "OK"  , format);
            }
            else if(sen.get_Status() == STATUS_FAIL){
                format.setPatternBackgroundColor(QColor(Qt::red));
                io->write(array[sen.get_Number()][sen.get_Type()].row_base , array[sen.get_Number()][sen.get_Type()].col_base +1, "FAIL" , format);
            }
            else{
                format.setPatternBackgroundColor(QColor(Qt::gray));
                io->write(array[sen.get_Number()][sen.get_Type()].row_base , array[sen.get_Number()][sen.get_Type()].col_base +1, "INIT" , format);
            }
            io->write(array[sen.get_Number()][sen.get_Type()].row_base , array[sen.get_Number()][sen.get_Type()].col_base + 2 , sen.get_B_raw() );
            io->write(array[sen.get_Number()][sen.get_Type()].row_base , array[sen.get_Number()][sen.get_Type()].col_base + 3 , sen.get_T_raw() );
            array[sen.get_Number()][sen.get_Type()].row_base++;


    }catch(...){


    }
}
*/
