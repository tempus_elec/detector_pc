#ifndef XLSXWRITER_H
#define XLSXWRITER_H

#include <QObject>
#include <QString>
#include <QtXlsx>
#include <xlsxdocument.h>
#include <sensor.h>

#ifndef CONSTANT_H
    #include "constant.h"
#endif

QTXLSX_USE_NAMESPACE

struct index_info{
    int row_base;
    int col_base;
};


class xlsxwriter : public QObject
{
    Q_OBJECT
public:
    explicit xlsxwriter(QObject *parent = nullptr);
    bool open();
    void close();
    QString getFileName();
    void makesheet();



private:
    QString path;
    int min;
    int max;
    Document *io;
    QVector< QVector <index_info>> array;

Q_SIGNALS:
    void save_signal_id(unsigned char index, unsigned char sub_index , int status , int b_raw , int t_raw , unsigned short zmdi_id);
    //void save_signal_id(sensor sen);

public Q_SLOTS:
    void setFileName(QString path);
    void change_min_index(int min);
    void change_max_index(int max);
    void saveData_by_id(unsigned char index , unsigned char sub_index , int status , int b_raw  ,int t_raw , unsigned short zmdi_id);
    //void saveData_by_id(sensor sen);
};

#endif // XLSXWRITER_H
