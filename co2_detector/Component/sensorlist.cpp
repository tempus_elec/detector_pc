#include "sensorlist.h"
#include <QDebug>
sensorList* sensorList::m_Instance = 0;

void sensorList::makeDoubleSensor()
{
    doublelist.append(new doublesensor(doublelist.size()));
    doublesensor* d_sensor = doublelist.last();

    linearlist.append(d_sensor->getSensor0());
    linearlist.append(d_sensor->getSensor1());


}

sensorList* sensorList::instance()
{
    static QMutex mutex;
    if(!m_Instance){
        mutex.lock();
        if(!m_Instance){
            m_Instance = new sensorList;
            mutex.unlock();
        }
    }
    return m_Instance;
}

QList<sensor*> sensorList::getLinearList()
{
    return linearlist;
}
QList<doublesensor*> sensorList::getDoubleList()
{
     return doublelist;
}

void sensorList::set_B_raw(int index , int sub_index , int b)
{
    if( index >= doublelist.size() || sub_index >= (REF + 1) ){
        throw "index_of_range";
        return;
    }
    int d_index = (index*2) + sub_index;
    linearlist.at(d_index)->set_B_raw(b);


}

void sensorList::set_T_raw(int index , int sub_index , int t)
{
    if( index >= doublelist.size() || sub_index >= REF + 1 ){
        throw "index_of_range";
        return;
    }
    int d_index = index*2 + sub_index;
    linearlist.at(d_index)->set_T_raw(t);
}

void sensorList::set_Status(int index , int sub_index , int status)
{
    if( index >= doublelist.size() || sub_index >= REF + 1 ){
        throw "index_of_range";
        return;
    }
    int d_index = index*2 + sub_index;
    linearlist.at(d_index)->set_Status(status);
}

void sensorList::set_Id(int index , int sub_index , unsigned short id)
{
    if( index >= doublelist.size() || sub_index >= REF + 1 ){
        throw "index_of_range";
        return;
    }

    int d_index = index*2 + sub_index;
    linearlist.at(d_index)->set_Id(id);
}

void sensorList::set_I2c_addr(int index , int sub_index , unsigned char addr)
{
    if( index >= doublelist.size() || sub_index >= REF + 1 ){
        throw "index_of_range";
        return;
    }

    int d_index = index*2 + sub_index;
    linearlist.at(d_index)->set_I2c_addr(addr);
}

int sensorList::get_B_raw(int index , int sub_index )
{
    if( index >= doublelist.size() || sub_index >= REF + 1 ){
        throw "index_of_range";
        return 0;
    }

    int d_index = index * 2 + sub_index;
    return linearlist.at(d_index)->get_B_raw();
}

int sensorList::get_T_raw(int index , int sub_index )
{
    if( index >= doublelist.size() || sub_index >= REF + 1 ){
        throw "index_of_range";
        return 0;
    }

    int d_index = index * 2 + sub_index;
    return linearlist.at(d_index)->get_T_raw();
}

int sensorList::get_Status(int index , int sub_index )
{
    if( index >= doublelist.size() || sub_index >= REF + 1 ){
        throw "index_of_range";
        return 0;
    }

    int d_index = index * 2 + sub_index;
    return linearlist.at(d_index)->get_Status();
}

unsigned short sensorList::get_Id(int index , int sub_index )
{
    if( index >= doublelist.size() || sub_index >= REF + 1 ){
        throw "index_of_range";
        return 0;
    }

    int d_index = index * 2 + sub_index;
    return linearlist.at(d_index)->get_Id();
}

unsigned char sensorList::get_I2c_addr(int index , int sub_index )
{
    if( index >= doublelist.size() || sub_index >= REF + 1 ){
        throw "index_of_range";
        return 0;
    }

    int d_index = index * 2 + sub_index;
    return linearlist.at(d_index)->get_I2c_addr();
}
int sensorList::get_LinearIndex(int index , int sub_index)
{


    return (index*2) + sub_index;
}

sensor sensorList::get_Sensor(int index , int sub_index)
{
    sensor sen;
    if( index >= doublelist.size() || sub_index >= REF + 1 ){
        throw "index_of_range";
        return sen;
    }
    int d_index = index * 2 + sub_index;
    sen = *(linearlist.at(d_index));

    return sen;

}
