#include "doublesensor.h"


doublesensor::doublesensor(int index)
{
    sensor0 = new sensor(index , CO2);
    sensor1 = new sensor(index , REF);
}


sensor* doublesensor::getSensor0() const
{
    return sensor0;
}
sensor* doublesensor::getSensor1() const
{
    return sensor1;
}
