#ifndef DELEGATE_1_H
#define DELEGATE_1_H

#include <QObject>
#include <page_1.h>
#include <QList>
#include <QFileDialog>
#include <QColor>
#include <QDir>
#include <QFile>
#include <QString>

#include <Thread/ReadWorker.h>
#include <Component/xlsxwriter.h>

#include <QFileInfo>
#include <qwt_plot_renderer.h>


class xlsxWindow;
class xlsxreader;
class Plot;
class gasSensor;


class controller_1 : public QObject
{
    Q_OBJECT
public:
    explicit controller_1(QObject *parent = nullptr);
    controller_1(page_1& page, ReadWorker &worker);

Q_SIGNALS:
    void setPath(QString path);
    void stateChanged(Qt::CheckState);
    void change_status(bool flag);
    void setEnableWidget(bool flag);
    void change_min(int min);
    void change_max(int max);

    void clear_signal();
    void init_sensor_signal();
    void read_signal(unsigned char start_index,unsigned int length);


public Q_SLOTS:
    void OpenDialog(int state);

    void click_read(bool);
    void set_port_status(bool port_opened);
    void received(unsigned int index,unsigned int sub_index );
    void set_Task_Status(bool flag);

private:
    int min_num = 0;
    int max_num = 0;
    bool isRunning = false;
    bool TaskIsRunning = false;
    bool port_status = false;

    page_1 *view;
    QList<sensor*> senslist;

    xlsxwriter record;

    QFileInfo info;

    QList<Plot*> graph_view;


    QwtPlotRenderer render;


};

#endif // controller_1_h
