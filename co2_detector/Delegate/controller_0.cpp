#include "controller_0.h"
#include <sensor.h>
#include <tablemodel.h>
#include <QModelIndex>
#include <QDir>
#include <QDebug>

controller_0::controller_0(QObject *parent) : QObject(parent)
{

}
controller_0::controller_0(page_0& page,ReadWorker& worker)
{

    for(int i=0;i<sensorList::instance()->getDoubleList().size();i++){
        d_rectlist.append(new DoubleRectModel(sensorList::instance()->getDoubleList().at(i)));
    }

    model = new TableModel(sensorList::instance()->getLinearList());

    QString path = QDir::currentPath();
    path += "/settings.ini";

    setting_path = path;
    QFile setfile(setting_path);

    if(!setfile.exists()){
        QSettings settings(setting_path , QSettings::IniFormat);
        settings.beginGroup("ID_VARIABLE");
        settings.setValue("MAX_ID",(unsigned int)1);
        settings.endGroup();
        settings.beginGroup("ADC_VARIABLE");
        settings.setValue("MAX_VOLTAGE" , QString::number(2.9));
        settings.endGroup();
    }


    page.setModel(model);
    page.setModel(d_rectlist);



    connect(this , SIGNAL(init_signal(unsigned char,unsigned int)),&worker,SLOT(push_array_init(unsigned char,unsigned int)));
    connect(this , SIGNAL(write_signal(unsigned char,unsigned int,unsigned short)),&worker,SLOT(push_array_write_id(unsigned char , unsigned int , unsigned short)));
    connect(this , SIGNAL(i2c_scan_signal(unsigned char,unsigned int)) , &worker , SLOT(push_array_scan(unsigned char,unsigned int)));
    connect(this , SIGNAL(i2c_addr_write_signal(unsigned char,unsigned int,unsigned char,unsigned char)) , &worker , SLOT(push_array_write_i2c_addr(unsigned char,unsigned int,unsigned char,unsigned char)));
    connect(this , SIGNAL(clear_signal()),&worker , SLOT(clear_queue()));

    connect(this , SIGNAL(update_adc_reference(float)) , &page , SLOT(setRefVoltage(float)));
    connect(this , SIGNAL(update_adc(int,float)) , &page , SLOT(setADCValue(int,float)));
    connect(this, SIGNAL(update_adc_status(int,QColor)) , &page , SLOT(setADCStatus(int,QColor)));
    connect(this , SIGNAL(setEneable(bool)) , &page , SLOT(setEnable(bool)));


    connect( &page , SIGNAL(click_chksensor(bool)),this,SLOT(click_chksensor(bool)));
    connect( &page , SIGNAL(click_write_id(bool)),this , SLOT(click_write_id(bool)));
    connect( &page , SIGNAL(click_i2c_scan(bool)) , this , SLOT(click_i2c_scan(bool)));
    connect( &page , SIGNAL(click_options(bool)),this,SLOT(click_options(bool)));
    connect( &page , SIGNAL(click_btn_voltage(bool)) , this , SLOT(click_voltage_ref(bool)));

    connect( &page , static_cast<void (page_0::*)(int)>(&page_0::min_box_changed) , this ,[=](int index){ min_num = index; });
    connect( &page , static_cast<void (page_0::*)(int)>(&page_0::max_box_changed) , this ,[=](int index){ max_num = index; });

    connect( &page , static_cast<void (page_0::*)(QString)>(&page_0::text_Changed) , this , [=](QString str){
        voltage_ref_str = str;
        voltage_ref = voltage_ref_str.toFloat();
    });

    connect( &worker , SIGNAL(update(unsigned int,unsigned int)), this , SLOT(received(unsigned int,unsigned int)));
    connect( &worker , SIGNAL(measure(unsigned int,unsigned int,bool)) , this , SLOT(set_border(unsigned int,unsigned int,bool)));


    connect( &worker , static_cast<void (ReadWorker::*)(int,int)>(&ReadWorker::update_adc) , this , [=](int index , int val){
        float voltage = val;
        voltage*=3.3/4095;
        if(voltage >= voltage_ref)
            Q_EMIT update_adc_status(index, QColor(Qt::red));
        else
            Q_EMIT update_adc_status(index, QColor(Qt::green));

        Q_EMIT update_adc(index ,voltage);
    });

    QSettings set(setting_path , QSettings::IniFormat);
    set.beginGroup("ADC_VARIABLE");
    voltage_ref_str = set.value("MAX_VOLTAGE").toString();
    bool isToFloat = false;
    voltage_ref = voltage_ref_str.toFloat(&isToFloat);
    Q_EMIT(update_adc_reference(voltage_ref));

    if( !isToFloat ){
        QMessageBox::critical(NULL,"page_0","Voltage Reference format error");
        set.setValue("MAX_VOLTAGE",QString::number(0.0));
        voltage_ref_str = QString::number(0.0);
        voltage_ref = 0.0;
    }



}
void controller_0::set_border(unsigned int index, unsigned int sub_index , bool isVisible)
{
    if( sub_index == CO2){
        d_rectlist[index]->setBorder0(isVisible);
    }
    if( sub_index == REF){
        d_rectlist[index]->setBorder1(isVisible);
    }
    model->border_visible(index, sub_index , isVisible);

}

void controller_0::received(unsigned int index,unsigned int sub_index )
{

    if(index == max_num && sub_index == REF)
    {
        isRunning = false;
        Q_EMIT change_status(isRunning);
        Q_EMIT setEneable(true);
    }

}

void controller_0::set_port_status(bool port_opened){

    port_status = port_opened;

    if(isRunning)
    {
        isRunning = false;
        Q_EMIT change_status(isRunning);
        Q_EMIT setEneable(true);
    }

}
void controller_0::set_Task_Status(bool flag)
{
    TaskIsRunning = flag;
}



/*
 * @brief page_0 event handler
 *
 */



void controller_0::click_chksensor(bool)
{
    if(!port_status){
         QMessageBox::critical(NULL,"serialport","serialport is not opened");
         return;
    }
    if(isRunning){
        QMessageBox::critical(NULL,"page0","Task is Running");
        return;
    }
    if(TaskIsRunning){
        QMessageBox::critical(NULL,"page0","Task is Running");
        return;
    }
    if(max_num < min_num){
        QMessageBox::critical(NULL,"page_0","invalid index");
        return;
    }

    unsigned int length = max_num - min_num + 1;
    unsigned char start = min_num;

    isRunning = true;

    Q_EMIT change_status(isRunning);
    Q_EMIT init_signal(start , length);
    Q_EMIT setEneable(false);

}

void controller_0::click_write_id(bool)
{
    if(!port_status){
         QMessageBox::critical(NULL,"serialport","serialport is not opened");
         return;
    }
    if(isRunning){
        QMessageBox::critical(NULL,"page0","Task is Running");
        return;
    }
    if(TaskIsRunning){
        QMessageBox::critical(NULL,"page0","Task is Running");
        return;
    }
    if(max_num < min_num){
        QMessageBox::critical(NULL,"page_0","invalid index");
        return;
    }

    unsigned short str_id = 0x0;

    QSettings settings(setting_path , QSettings::IniFormat);
    settings.beginGroup("ID_VARIABLE");
    str_id = (unsigned short)settings.value("MAX_ID").toUInt();

    dialog_writeid.id_Changed(str_id);

    if(dialog_writeid.exec()){

        unsigned char start = min_num;
        unsigned int length = max_num - min_num + 1;
        unsigned short start_id = dialog_writeid.get_start_id();

        isRunning = true;

        Q_EMIT change_status(isRunning);
        Q_EMIT write_signal(start , length , start_id);
        Q_EMIT setEneable(false);

        start_id += length;
        settings.setValue("MAX_ID" , start_id);
        settings.endGroup();

    }

}
void controller_0::click_options(bool)
{
    if(!port_status){
         QMessageBox::critical(NULL,"serialport","serialport is not opened");
         return;
    }
    if(isRunning){
        QMessageBox::critical(NULL,"page0","Task is Running");
        return;
    }
    if(TaskIsRunning){
        QMessageBox::critical(NULL,"page0","Task is Running");
        return;
    }
    if(max_num < min_num){
        QMessageBox::critical(NULL,"page_0","invalid index");
        return;
    }

    if(dialog_options.exec()){

        isRunning = true;
        unsigned int length = max_num - min_num + 1;
        unsigned char start = min_num;
        OptionDialog::Settings settings = dialog_options.settings();

        Q_EMIT change_status(isRunning);
        Q_EMIT i2c_addr_write_signal(start , length , settings.co2_addr, settings.ref_addr);
        Q_EMIT setEneable(false);

    }


}
void controller_0::click_i2c_scan(bool)
{
    if(!port_status){
         QMessageBox::critical(NULL,"serialport","serialport is not opened");
         return;
    }
    if(isRunning){
        QMessageBox::critical(NULL,"page0","Task is Running");
        return;
    }
    if(TaskIsRunning){
        QMessageBox::critical(NULL,"page0","Task is Running");
        return;
    }

    isRunning = true;
    unsigned int length = max_num - min_num + 1;
    unsigned char start = min_num;


    Q_EMIT change_status(isRunning);
    Q_EMIT i2c_scan_signal(start , length);
    Q_EMIT setEneable(false);

}
void controller_0::click_voltage_ref(bool)
{
    if(isRunning){
        QMessageBox::critical(NULL,"page0","Task is Running");
        return;
    }
    if(TaskIsRunning){
        QMessageBox::critical(NULL,"page0","Task is Running");
        return;
    }
    QSettings set(setting_path , QSettings::IniFormat);
    set.beginGroup("ADC_VARIABLE");
    set.setValue("MAX_VOLTAGE",QString::number(voltage_ref));
    set.endGroup();

    Q_EMIT(update_adc_reference(voltage_ref));
}
