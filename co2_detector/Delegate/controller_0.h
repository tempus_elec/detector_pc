#ifndef DELEGATE_0_H
#define DELEGATE_0_H

#include <QObject>
#include <QList>
#include <QMessageBox>

#include <page_0.h>
#include <ReadWorker.h>
#include <QFile>
#include <QSettings>
#include <optiondialog.h>
#include <writeiddialog.h>

#include <doublerectmodel.h>
#include <sensorlist.h>


class SensorTableModel;
class gasSensor;



class controller_0 : public QObject
{
    Q_OBJECT
public:
    explicit controller_0(QObject *parent = 0);
    controller_0(page_0& page, ReadWorker& worker);


Q_SIGNALS:

    void init_signal(unsigned char start_index,unsigned int length);
    void write_signal(unsigned char start_index , unsigned int length , unsigned short id );
    void i2c_scan_signal(unsigned char start_index , unsigned int length);
    void i2c_addr_write_signal(unsigned char start_index , unsigned int length , unsigned char co2_addr , unsigned char ref_addr);
    void clear_signal();
    void change_status(bool flag);
    void update_adc(int index , float value);
    void update_adc_status(int index , QColor color);
    void update_adc_reference(float vol);

    void setEneable(bool flag);


public Q_SLOTS:
    void received(unsigned int index,unsigned int sub_index);
    void set_border(unsigned int index, unsigned int sub_index , bool isVisible);

    void click_i2c_scan(bool);
    void click_chksensor(bool);
    void click_write_id(bool);
    void click_options(bool);
    void click_voltage_ref(bool);


    void set_port_status(bool port_opened);
    void set_Task_Status(bool flag);




private:
    TableModel *model;

    QList<DoubleRectModel*> d_rectlist;
    QString setting_path;


    int min_num = 0;
    int max_num = 0;

    OptionDialog dialog_options;
    WriteIdDialog dialog_writeid;
    QString voltage_ref_str;
    float voltage_ref;

    bool port_status = false;
    bool isRunning = false;
    bool TaskIsRunning = false;

};

#endif // controller_0_h
