#include "controller_1.h"
#include <QMessageBox>

#include <sensorlist.h>
#include <sensor.h>
#include <qwt/plot.h>


controller_1::controller_1(QObject *parent) : QObject(parent)
{

}

controller_1::controller_1( page_1& page,ReadWorker& worker)
{
    connect( &page , SIGNAL(state_changed(int)), this , SLOT(OpenDialog(int)));

    connect( &page , static_cast<void (page_1::*)(int)>(&page_1::min_box_changed), this , [=](int index){ min_num = index; });
    connect( &page , static_cast<void (page_1::*)(int)>(&page_1::max_box_changed), this , [=](int index){ max_num = index;});
    connect( &page , SIGNAL(max_box_changed(int)) , &record , SLOT(change_max_index(int)));
    connect( &page , SIGNAL(min_box_changed(int)) , &record , SLOT(change_min_index(int)));

    connect( &page , SIGNAL(click_read(bool)),this,SLOT(click_read(bool)));

    connect( this , SIGNAL(read_signal(unsigned char,unsigned int)),&worker,SLOT(push_array_read_no_filter(unsigned char,unsigned int)));
    connect( this , SIGNAL(clear_signal()),&worker , SLOT(clear_queue()));
    connect( this , SIGNAL(init_sensor_signal()) , &worker , SLOT(init_list()));

    connect( this , SIGNAL(stateChanged(Qt::CheckState)), &page , SLOT(setcheckState(Qt::CheckState)));
    connect( this , SIGNAL(setPath(QString)), &page , SLOT(showPath(QString)));
    connect( this , SIGNAL(change_status(bool)), &page, SLOT(setbtnText(bool)));
    connect( this , SIGNAL(setEnableWidget(bool)), &page,SLOT(setEnable(bool)));
    connect( this , SIGNAL(setPath(QString)), &record, SLOT(setFileName(QString)));

    connect( &worker , SIGNAL(update(unsigned int,unsigned int)), this , SLOT(received(unsigned int,unsigned int)));

    graph_view.append(new Plot);
    graph_view.append(new Plot);

    graph_view[CO2]->setTitle("CO2");
    graph_view[REF]->setTitle("REF");

    page.addGraphView(graph_view[CO2]);
    page.addGraphView(graph_view[REF]);


    int length = sensorList::instance()->getDoubleList().size();

    for(int i=0;i<length;i++){
        QColor color;
        color.setRgb(20 + 2*i,250 - (2*i) , 20 + 2*i);
        graph_view[CO2]->makeCurve(QString::number(i));
        graph_view[CO2]->setPen(i , color , 1.0);
        graph_view[REF]->makeCurve(QString::number(i));
        graph_view[REF]->setPen(i , color , 1.0);
    }
    Q_EMIT setPath(QString());
}


void controller_1::OpenDialog(int state)
{

    switch(state){
    case Qt::Unchecked:
        Q_EMIT setPath(QString());
        break;

    case Qt::Checked:
        QString str = QFileDialog::getSaveFileName(nullptr,QString(),QDir::currentPath(),"excel files(*.xlsx)");
        if(str.isEmpty()){
            Q_EMIT stateChanged(Qt::Unchecked);
        }
        Q_EMIT setPath(str);
        break;

    }

}


void controller_1::click_read(bool)
{
    if(!port_status){
         QMessageBox::critical(NULL,"serialport","serialport is not opened");
         return;
    }
    if(max_num < min_num){
        QMessageBox::critical(NULL,"page_1","invalid index");
        return;
    }
    if(TaskIsRunning){
        QMessageBox::critical(NULL,"page1","Task is Running");
        return;
    }

    isRunning = !isRunning;
    Q_EMIT change_status(isRunning);
    Q_EMIT setEnableWidget(!isRunning);

    if(!isRunning){

         record.close();
         if(!record.getFileName().isEmpty()){

             QString filename = record.getFileName();
             filename.remove(QString(".xlsx"));

             QString co2_img = filename + "_CO2.jpeg";
             QString ref_img = filename + "_REF.jpeg";

             render.renderDocument(graph_view[CO2],co2_img,"jpeg",QSize(500,500));
             render.renderDocument(graph_view[REF],ref_img,"jpeg",QSize(500,500));

         }
         Q_EMIT stateChanged(Qt::Unchecked);
         Q_EMIT setPath(QString());
         Q_EMIT clear_signal();
    }
    else{
         record.open();
         graph_view[CO2]->clear();
         graph_view[REF]->clear();


         unsigned int start_index = min_num;
         unsigned int length = max_num - min_num + 1;

         Q_EMIT init_sensor_signal();
         Q_EMIT read_signal( start_index , length );
    }

}
void controller_1::received(unsigned int index,unsigned int sub_index )
{
    if(isRunning){


        int status = sensorList::instance()->get_Status(index , sub_index);
        int b_raw =  sensorList::instance()->get_B_raw(index , sub_index);
        int t_raw = sensorList::instance()->get_T_raw(index , sub_index);
        unsigned short zmdi_id = sensorList::instance()->get_Id( index , sub_index);

        if(status == STATUS_OK && b_raw != 0){
             graph_view[sub_index]->addData(index,b_raw);
        } else {   
             graph_view[sub_index]->addEmpty(index);
        }

        Q_EMIT record.save_signal_id( index , sub_index , status , b_raw , t_raw , zmdi_id );

        if(index == max_num && sub_index == REF ) {
            unsigned char start_index = min_num;
            unsigned int length = max_num - min_num + 1;
            Q_EMIT read_signal(start_index , length);
        }
    }
}
void controller_1::set_port_status(bool port_opened)
{
    if(port_status && isRunning){
        click_read(true);
    }
    port_status = port_opened;
}
void controller_1::set_Task_Status(bool flag)
{
    TaskIsRunning = flag;
}

