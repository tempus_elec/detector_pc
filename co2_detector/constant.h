#ifndef CONSTANT_H
#define CONSTANT_H

#endif // CONSTANT_H

enum{
    STATUS_OK = 0,
    STATUS_FAIL,
    STATUS_INIT
};

enum{
    CO2 = 0,
    REF
};


#define ToByte(a , b)      ((a >> (b*8)) & 0xff)
#define ToUInt(a,b,c,d)		( (a & 0xff) << 24 | (b & 0xff) << 16 | (c & 0xff) << 8 | (d & 0xff))
#define ToUShort( a , b)    ( (a & 0xff) << 8 | (b & 0xff))
