#ifndef _SIGNAL_DATA_H_
#define _SIGNAL_DATA_H_ 1

#include <qrect.h>
#include <QVector>
class SignalData
{
public:
    static SignalData &instance();
    static SignalData &getEnvInstance();

    void append( const QPointF &pos );
    void clearStaleValues( double min );
    void clearValues();
    void pop();

    int size() const;
    QPointF& value( int index ) const;
    QVector<QPointF>& index();

    QRectF boundingRect() const;

    void lock();
    void unlock();

// private:
    SignalData();
    SignalData( const SignalData & );
    SignalData &operator=( const SignalData & );

    virtual ~SignalData();



    private:

    class PrivateData;
    PrivateData *d_data;
};

#endif
