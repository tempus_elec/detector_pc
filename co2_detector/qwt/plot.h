#ifndef PLOT_H
#define PLOT_H
#include "qwt_plot.h"
#include "qwt_plot_marker.h"
#include "qwt_plot_grid.h"
#include "qwt_plot_curve.h"
#include "qwt_plot_directpainter.h"
#include "curvedata.h"
#include "qwt_scale_map.h"
#include "qwt_text.h"


#include <qwt_system_clock.h>


#define INIT_AXIS_VALUE  0xffffffff
#define MAX(a,b)    a>b?a:b
#define MIN(a,b)    a>b?b:a
#define MAX_COUNT   100

class Plot : public QwtPlot
{
    Q_OBJECT
public:
    Plot( QWidget * = NULL );


    void clear();
    int makeCurve(QString title);
    void deleteCurve();
    void setPen(int i, const QColor &color, qreal width);

protected:
    virtual void replot();
    virtual void showEvent( QShowEvent * );

private:
    void updateCurve();
    void incrementInterval();

public Q_SLOTS:


    void addData(int i,float temp);
    void addEmpty(int i);




public:

    QwtPlotMarker *d_origin;

    QwtPlotCurve *d_curve;
    QwtPlotCurve *d_envcurve;

    QwtPlotCurve *d_curves[MAX_COUNT];



    QwtSystemClock d_clock;

    QwtPlotDirectPainter *d_directPainter;
    CurveData* curves[MAX_COUNT];
    CurveData* pointer_curves[MAX_COUNT];
    QwtScaleMap xMap[MAX_COUNT];
    QwtScaleMap yMap[MAX_COUNT];
    QRectF br[MAX_COUNT];
    QRect ClipRect[MAX_COUNT];
    unsigned int d_count[MAX_COUNT];
    unsigned int reset_count = 0;


    int d_paintedPoints[MAX_COUNT];

    QwtInterval d_interval;
    int d_timerId;


    float axisy_min_value;
    float axisy_max_value;

    int drawmode;
   // const int MAXIMIM_LINE = 5;
private:
    int CurveCount;

};

#endif // PLOT_H
