#include "curvedata.h"
#include "signaldata.h"


CurveData::CurveData()
{
    data = new SignalData();

}
/*
const SignalData &CurveData::values() const
{

     return *data;
}
*/
SignalData &CurveData::values()
{

     return *data;
}

QPointF CurveData::sample( size_t i ) const
{
      return data->value(i);
}

size_t CurveData::size() const
{
      return data->size();
}

QRectF CurveData::boundingRect() const
{
      return data->boundingRect();
}

