#include "plot.h"

#include "signaldata.h"
#include <qwt_plot_grid.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_directpainter.h>
#include <qwt_curve_fitter.h>
#include <qwt_painter.h>
#include <qevent.h>
#include "mainwindow.h"
#include <qwt_legend.h>

#include <QDebug>

class Canvas: public QwtPlotCanvas
{
public:
    Canvas( QwtPlot *plot = NULL ):
        QwtPlotCanvas( plot )
    {
        // The backing store is important, when working with widget
        // overlays ( f.e rubberbands for zooming ).
        // Here we don't have them and the internal
        // backing store of QWidget is good enough.

        setPaintAttribute( QwtPlotCanvas::BackingStore, false );
        setBorderRadius( 10 );

        if ( QwtPainter::isX11GraphicsSystem() )
        {
#if QT_VERSION < 0x050000
            // Even if not liked by the Qt development, Qt::WA_PaintOutsidePaintEvent
            // works on X11. This has a nice effect on the performance.

            setAttribute( Qt::WA_PaintOutsidePaintEvent, true );
#endif

            // Disabling the backing store of Qt improves the performance
            // for the direct painter even more, but the canvas becomes
            // a native window of the window system, receiving paint events
            // for resize and expose operations. Those might be expensive
            // when there are many points and the backing store of
            // the canvas is disabled. So in this application
            // we better don't disable both backing stores.

            if ( testPaintAttribute( QwtPlotCanvas::BackingStore ) )
            {
                setAttribute( Qt::WA_PaintOnScreen, true );
                setAttribute( Qt::WA_NoSystemBackground, true );
            }
        }

        setupPalette();
    }

private:
    void setupPalette()
    {
        QPalette pal = palette();

#if QT_VERSION >= 0x040400
        /*
        QLinearGradient gradient;
        gradient.setCoordinateMode( QGradient::StretchToDeviceMode );
        gradient.setColorAt( 0.0, QColor( 0, 49, 110 ) );
        gradient.setColorAt( 1.0, QColor( 0, 87, 174 ) );
        */

        pal.setBrush( QPalette::Window, QBrush( Qt::white ) );
#else
        pal.setBrush( QPalette::Window, QBrush( color ) );
#endif

     //    QPalette::WindowText is used for the curve color
       // pal.setColor( QPalette::WindowText, Qt::black );

        setPalette( pal );
    }
};

Plot::Plot(  QWidget *parent ) :
d_interval( 0.0, 10.0 )
{

    setAutoReplot( false );
    setCanvas(new Canvas());

    plotLayout()->setAlignCanvasToScales( true );

    axisy_min_value = 0;//INIT_AXIS_VALUE;
    axisy_max_value = 10;//INIT_AXIS_VALUE;
     
    this->enableAxis(QwtPlot::yLeft);
    this->enableAxis(QwtPlot::xBottom);
   // this->enableAxis(QwtPlot::yRight);
     
    setAxisTitle( QwtPlot::xBottom, "Time [s]" );
    setAxisScale( QwtPlot::xBottom, d_interval.minValue(),d_interval.maxValue()  );
    setAxisAutoScale( QwtPlot::yLeft,true);

    d_directPainter = new QwtPlotDirectPainter();

    CurveCount = 0;


    QwtPlotGrid *grid = new QwtPlotGrid();
    grid->setPen( Qt::black, 0.0, Qt::DotLine );
    grid->enableX( true );
    grid->enableXMin( true );
    grid->enableY( true );
    grid->enableYMin( true );

    grid->attach( this );

    this->insertLegend(new QwtLegend());


}

int Plot::makeCurve(QString title)
{
    if(CurveCount > MAX_COUNT)
        return -1;

    curves[CurveCount] = new CurveData();
    d_curves[CurveCount] = new QwtPlotCurve(title);
    d_curves[CurveCount]->setStyle( QwtPlotCurve::Lines );
    d_curves[CurveCount]->setRenderHint( QwtPlotItem::RenderAntialiased, true );
    d_curves[CurveCount]->setPaintAttribute( QwtPlotCurve::ClipPolygons, false );
    d_curves[CurveCount]->setData( curves[CurveCount] );
    d_curves[CurveCount]->setAxes( QwtPlot::xBottom, QwtPlot::yLeft );


    pointer_curves[CurveCount] = static_cast<CurveData*>(d_curves[CurveCount]->data());
    d_curves[CurveCount]->attach(this);
    d_paintedPoints[CurveCount] = 0;
    d_count[CurveCount] = 0;
    return CurveCount++;


}
void Plot::deleteCurve()
{
    if(CurveCount == 0)
        return ;

    delete curves[CurveCount];
    delete d_curves[CurveCount];

    d_count[CurveCount] = 0;
    d_paintedPoints[CurveCount] = 0;
    pointer_curves[CurveCount] = NULL;
    CurveCount--;
}

void Plot::setPen(int i, const QColor &color, qreal width = 0.0)
{

    if(i >= CurveCount)
        return;

    d_curves[i]->setPen( color, width );

}
void Plot::clear()
{
    for(int i=0;i<CurveCount;i++){
        curves[i]->values().clearValues();
        d_count[i] = 0;
        d_paintedPoints[i] = 0;
    }

    d_interval = QwtInterval( 0 , 10.0 );
    reset_count = 0;

    setAxisScale( QwtPlot::xBottom, d_interval.minValue(),d_interval.maxValue()  );

}


void Plot::replot()
{
    for(int i=0;i<CurveCount;i++)
    {
        pointer_curves[i] = static_cast<CurveData*>(d_curves[i]->data());
        pointer_curves[i]->values().lock();
    }
    QwtPlot::replot();
    for(int i=0;i<CurveCount;i++)
    {
       // d_paintedPoints[i] = pointer_curves[i]->size();
        pointer_curves[i]->values().unlock();
    }

}

void Plot::updateCurve()
{
    float min,max;

    QRect ClipRect[MAX_COUNT];
    int numPoints[MAX_COUNT];
    bool isUpdate = false;
    for(int i=0;i<CurveCount;i++)
    {
       pointer_curves[i] = static_cast<CurveData *>(d_curves[i]->data());
       pointer_curves[i]->values().lock();
        numPoints[i] = pointer_curves[i]->size();
        if(numPoints[i] > d_paintedPoints[i])
            isUpdate = true;
    }


    if(isUpdate)
    {
        const bool doClip = !canvas()->testAttribute(Qt::WA_PaintOnScreen);
        if( doClip )
        {
            QRect unite;

            xMap[CurveCount-1] = canvasMap(d_curves[CurveCount-1]->xAxis());
            yMap[CurveCount-1] = canvasMap(d_curves[CurveCount-1]->yAxis());
            int size = pointer_curves[CurveCount-1]->size();
            br[CurveCount - 1] = qwtBoundingRect(*pointer_curves[CurveCount-1],d_paintedPoints[CurveCount-1] - 1,size - 1);

            ClipRect[CurveCount-1] = QwtScaleMap::transform(xMap[CurveCount-1], yMap[CurveCount-1], br[CurveCount-1]).toRect();
            unite = ClipRect[CurveCount-1];



            for(int i=0;i<CurveCount - 1;i++)
            {
                xMap[i] = canvasMap(d_curves[i]->xAxis());
                yMap[i] = canvasMap(d_curves[i]->yAxis());
                int size = pointer_curves[i]->size();
                br[i] = qwtBoundingRect(*pointer_curves[i],d_paintedPoints[i] - 1,size - 1);
                ClipRect[i] = QwtScaleMap::transform(xMap[i], yMap[i], br[i]).toRect();
                unite = unite.united(ClipRect[i]);
            }

            d_directPainter->setClipRegion(unite);


            for(int i=0;i<CurveCount;i++){
                d_directPainter->drawSeries( d_curves[i], d_paintedPoints[i] - 1, numPoints[i] - 1);
                d_paintedPoints[i] = numPoints[i];

            }
        }
    }

    QRectF bounding = pointer_curves[CurveCount-1]->boundingRect();
    for(int i=0;i<CurveCount;i++){
        bounding = bounding.united(pointer_curves[i]->boundingRect());
        pointer_curves[i]->values().unlock();

    }
        max = bounding.bottom();
        min = 0;



    replot();


}
void Plot::addData(int k,float temp)
{
    QPointF temporary = QPointF(d_count[k] ,temp);
    curves[k]->values().append(temporary);

    if ( d_count[k] > d_interval.maxValue() ){
        incrementInterval();
    }
    updateCurve();
    d_count[k]++;

    return ;
}
void Plot::addEmpty(int i)
{
   if ( d_count[i] > d_interval.maxValue() ){
        incrementInterval();
    }
    d_count[i]++;
}

void Plot::incrementInterval()
{
    d_interval = QwtInterval(0, d_interval.maxValue() + 1);
    setAxisScale( QwtPlot::xBottom , d_interval.minValue() , d_interval.maxValue()  );
    /*
    if(d_interval.width() < 300.0){
        d_interval = QwtInterval(0, d_interval.maxValue() + 1);
        setAxisScale( QwtPlot::xBottom , d_interval.minValue() , d_interval.maxValue()  );

    } else{
        d_interval = QwtInterval(d_interval.minValue() + 1, d_interval.maxValue() + 1);
        setAxisScale( QwtPlot::xBottom , d_interval.minValue() , d_interval.maxValue()  );
        reset_count++;
        if(reset_count >= 300)
        {
            for(int i=0;i<CurveCount;i++)
            {
                pointer_curves[i] = static_cast<CurveData *>(d_curves[i]->data());
                pointer_curves[i]->values().clearStaleValues(d_interval.minValue() - 10);

            }
            reset_count = 0;

        }

    }
    */




    //data->values().clearStaleValues(d_interval.minValue() );


    /*
    d_interval = QwtInterval( d_interval.maxValue(),
        d_interval.maxValue() + d_interval.width() );

    CurveData *data = static_cast<CurveData *>( d_curve->data() );
    data->values().clearStaleValues( d_interval.minValue() );

    // To avoid, that the grid is jumping, we disable
    // the autocalculation of the ticks and shift them
    // manually instead.

    QwtScaleDiv scaleDiv = axisScaleDiv( QwtPlot::xBottom );
    scaleDiv.setInterval( d_interval );

    for ( int i = 0; i < QwtScaleDiv::NTickTypes; i++ )
    {
        QList<double> ticks = scaleDiv.ticks( i );
        for ( int j = 0; j < ticks.size(); j++ )
            ticks[j] += d_interval.width();
        scaleDiv.setTicks( i, ticks );
    }
    setAxisScaleDiv( QwtPlot::xBottom, scaleDiv );



    d_paintedPoints = 0;
    */



}
void Plot::showEvent( QShowEvent * )
{
    replot();
}



