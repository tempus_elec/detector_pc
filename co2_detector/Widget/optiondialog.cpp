#include "optiondialog.h"
#include "ui_optiondialog.h"
#include <QDebug>

OptionDialog::OptionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionDialog)
{
    ui->setupUi(this);
    connect(ui->btnapply , SIGNAL(clicked(bool)) , this , SLOT(apply()));
    connect( ui->btndefault , SIGNAL(clicked(bool)) , this , SLOT(setdefault()));

    QStringList strlist;
    strlist<<"0"<<"1"<<"2"<<"3";
    for(int i=8;i<128;i++)
        strlist<<QString::number(i);
    ui->co2_combo->addItems(strlist);
    ui->ref_combo->addItems(strlist);

    setdefault();
}

OptionDialog::~OptionDialog()
{
    delete ui;
}
void OptionDialog::apply()
{
    updateSettings();
    this->accept();
    //hide();


}
void OptionDialog::updateSettings()
{
    unsigned char co2_addr = (unsigned char)(ui->co2_combo->currentText().toUShort());
    unsigned char ref_addr = (unsigned char)(ui->ref_combo->currentText().toUShort());
    currentSettings.co2_addr = co2_addr;
    currentSettings.ref_addr = ref_addr;



}
void OptionDialog::setdefault()
{
    currentSettings.co2_addr = 0x0;
    currentSettings.ref_addr = 0x0;


    ui->co2_combo->setCurrentIndex(0);
    ui->ref_combo->setCurrentIndex(0);


}
OptionDialog::Settings OptionDialog::settings() const
{
    return currentSettings;
}
