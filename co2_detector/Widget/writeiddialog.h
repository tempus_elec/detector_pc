#ifndef WRITEIDDIALOG_H
#define WRITEIDDIALOG_H

#include <QDialog>

namespace Ui {
class WriteIdDialog;
}

class WriteIdDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WriteIdDialog(QWidget *parent = 0);
    WriteIdDialog(unsigned short id);
    ~WriteIdDialog();

public Q_SLOTS:
    void id_Changed(unsigned short id);
    void apply();
    void setdefault();
    unsigned short get_start_id();

private:
    Ui::WriteIdDialog *ui;
    unsigned short start_id = 0x0;
};

#endif // WRITEIDDIALOG_H
