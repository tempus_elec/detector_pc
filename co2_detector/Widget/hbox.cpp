#include "hbox.h"
#include <QPaintEvent>
#include <QPainter>
#include <QRect>
#include <QPen>

HBox::HBox() : QGroupBox(nullptr)
{
    this->setLayout(&pannel);
}

void HBox::addWidget(QWidget* item)
{
    pannel.addWidget(item);
    pannel.setSpacing(20);
}
