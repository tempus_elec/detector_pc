#ifndef HBOX_H
#define HBOX_H

#include <QGroupBox>
#include <QHBoxLayout>
#include <QWidget>
class HBox : public QGroupBox
{
    Q_OBJECT
public:
    HBox();
    void addWidget(QWidget* item);



    QHBoxLayout pannel;
};

#endif // HBOX_H
