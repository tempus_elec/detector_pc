#include "writeiddialog.h"
#include "ui_writeiddialog.h"

WriteIdDialog::WriteIdDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WriteIdDialog)
{
    ui->setupUi(this);
    connect(ui->btnapply , SIGNAL(clicked(bool)) , this , SLOT(apply()));
    connect(ui->btndefault , SIGNAL(clicked(bool)) , this , SLOT(setdefault()));
}

WriteIdDialog::~WriteIdDialog()
{
    delete ui;
}
void  WriteIdDialog::apply()
{
    start_id = ui->spinBox->value();
    this->accept();
}

void WriteIdDialog::id_Changed(unsigned short id)
{
    start_id = id;
    ui->spinBox->setValue(start_id);
}
void WriteIdDialog::setdefault()
{
    ui->spinBox->setValue(start_id);
}
unsigned short WriteIdDialog::get_start_id()
{
    return start_id;
}
