#ifndef OPTIONDIALOG_H
#define OPTIONDIALOG_H

#include <QDialog>

namespace Ui {
class OptionDialog;
}

class OptionDialog : public QDialog
{
    Q_OBJECT

public:
    struct Settings{

        unsigned char co2_addr;
        unsigned char ref_addr;
    };

    explicit OptionDialog(QWidget *parent = 0);
    ~OptionDialog();
    Settings settings() const;
Q_SIGNALS:
    void i2c_write_start(bool);

public Q_SLOTS:
    void apply();
    void setdefault();
    void updateSettings();

private:
    Ui::OptionDialog *ui;
    Settings currentSettings;
    int co2_index = 0;
    int ref_index = 0;
};

#endif // OPTIONDIALOG_H
