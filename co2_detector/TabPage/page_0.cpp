#include "page_0.h"
#include "ui_page_0.h"
#include <QTableWidgetItem>
#include "tableview.h"
#include "tablemodel.h"

page_0::page_0(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::page_0)
{
    ui->setupUi(this);

    dataView = new TableView;

    for(int i=0;i<100;i++)
    {
        ui->maxBox->addItem(QString("%1").arg(i));
        ui->minBox->addItem(QString("%1").arg(i));
    }


    ui->edit_voltage->setValidator(new QDoubleValidator(0.0 , 3.3 , 2 ));


    scan.setText("I2C Scan");
    chksensor.setText("CHKSENSOR");
    btn_setting.setText("READSENSOR");
    btn_write_id.setText("WRITE_ID");
    btn_options.setText("OPTIONS");





    ui->btnlayout->addWidget(&chksensor);
    ui->btnlayout->addWidget(&btn_write_id);
    ui->btnlayout->addSpacing(50);
    ui->btnlayout->addWidget(&scan);
    ui->btnlayout->addWidget(&btn_options);


    ui->btnlayout->addSpacing(950);

    ui->horizontalLayout->addWidget(dataView);

    dataView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    dataView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    ui->horizontalLayout->setStretch( 0 , 5);
    ui->horizontalLayout->setStretch( 1 , 4);

    ui->adc_view->setMinimumHeight(10);

    for(int i=0;i<ui->adc_view->columnCount();i++){
        QTableWidgetItem *header = new QTableWidgetItem;
        QTableWidgetItem *cell = new QTableWidgetItem;
        QTableWidgetItem *cell_1 = new QTableWidgetItem;
        header->setText("ADC " + QString::number(i));
        ui->adc_view->setItem(0,i,cell);
        ui->adc_view->setItem(1 , i , cell_1);
        ui->adc_view->setHorizontalHeaderItem(i,header);
    }

    connect(ui->minBox , SIGNAL(currentIndexChanged(int)),this , SIGNAL(min_box_changed(int)));
    connect(ui->maxBox , SIGNAL(currentIndexChanged(int)),this , SIGNAL(max_box_changed(int)));

    connect(&scan , SIGNAL(clicked(bool)),this,SIGNAL(click_i2c_scan(bool)));
    connect(&chksensor,SIGNAL(clicked(bool)),this,SIGNAL(click_chksensor(bool)));
    connect(&btn_setting,SIGNAL(clicked(bool)),this,SIGNAL(click_read(bool)));
    connect(&btn_write_id,SIGNAL(clicked(bool)),this,SIGNAL(click_write_id(bool)));
    connect(&btn_options , SIGNAL(clicked(bool)),this,SIGNAL(click_options(bool)));
    connect(ui->btn_voltage , SIGNAL(clicked(bool)) , this , SIGNAL(click_btn_voltage(bool)));

    connect(ui->edit_voltage,SIGNAL(textEdited(QString)) , this , SIGNAL(text_Changed(QString)));
}

page_0::~page_0()
{
    delete ui;
}

QTableView* page_0::getTableView()
{
    return dataView;
}
void page_0::setModel(QList<DoubleRectModel*> list)
{
    Q_FOREACH(const DoubleRectModel* get , list){
        doublerectlist.append(new DoubleRectView(get));
    }

    int count = 0;

    int hbox_count = doublerectlist.size() / 10;
    int rect_count = 0;
    int n_rect_count = 0;
    for(int i=0; i<hbox_count;i++){

        boxlist.append(new HBox());

        n_rect_count = rect_count + 10;

        while( (rect_count < doublerectlist.size()) && (rect_count < n_rect_count)){
            boxlist.at(i)->addWidget(doublerectlist.at(rect_count));
            rect_count++;

        }
        boxlist.at(i)->setTitle("ADC"+QString::number(i));
        ui->vLayout->addWidget(boxlist.at(i));

    }



}
void page_0::setModel(TableModel *tmodel)
{
    dataView->setModel(tmodel);
}
void page_0::setRefVoltage(float vol)
{
    ui->label_voltage->setText(QString::number(vol));
}

void page_0::setADCValue(int i , float val)
{
    ui->adc_view->item(0 , i)->setText(QString::number(val) + " V");

}
void page_0::setADCStatus(int i , QColor color)
{
    if(color == QColor(Qt::red))
        ui->adc_view->item(1 , i)->setText("fail");
    else
        ui->adc_view->item(1 , i)->setText("pass");

    ui->adc_view->item(1 , i)->setBackgroundColor(color);
}
void page_0::setEnable(bool enabled)
{
    ui->minBox->setEnabled(enabled);
    ui->maxBox->setEnabled(enabled);
}
