#include "page_1.h"
#include "ui_page_1.h"
#include "qwt/plot.h"
#include "constant.h"
#include "sensorlist.h"

#include <QDebug>

page_1::page_1(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::page_1)
{
    ui->setupUi(this);



    for(int i=0;i<100;i++)
    {
        ui->MaxBox->addItem(QString("%1").arg(i));
        ui->MinBox->addItem(QString("%1").arg(i));
    }

    connect(ui->MinBox , SIGNAL(currentIndexChanged(int)),this , SIGNAL(min_box_changed(int)));
    connect(ui->MaxBox , SIGNAL(currentIndexChanged(int)),this , SIGNAL(max_box_changed(int)));
    connect(ui->checkbox_log , SIGNAL(stateChanged(int)),this , SIGNAL(state_changed(int)));
    connect(ui->btn_read , SIGNAL(clicked(bool)),this , SIGNAL(click_read(bool)));
}

page_1::~page_1()
{
    delete ui;
}
void page_1::addGraphView(QWidget *plot)
{
    ui->graphlayout->addWidget(plot);
}

QCheckBox* page_1::getLogCheck()
{
    return ui->checkbox_log;
}
QLabel* page_1::getPathLabel()
{
    return ui->filepath;
}
void page_1::showPath(QString path)
{
    ui->filepath->setText(path);
}
void page_1::setcheckState(Qt::CheckState state)
{
    ui->checkbox_log->setCheckState(state);
}
void page_1::setbtnText(bool onClicked)
{
    if(onClicked){
        ui->btn_read->setText("Stop");
    } else{
        ui->btn_read->setText("Read");
    }
}
void page_1::setEnable(bool enabled)
{
    ui->checkbox_log->setEnabled(enabled);
    ui->MinBox->setEnabled(enabled);
    ui->MaxBox->setEnabled(enabled);
}
