#ifndef PAGE_1_H
#define PAGE_1_H

#include <QWidget>
#include <QCheckBox>
#include <QLabel>
#include <QString>
#include <QColor>
#include <QList>
#include <QPen>


namespace Ui {
class page_1;
}

class page_1 : public QWidget
{
    Q_OBJECT

public:
    explicit page_1(QWidget *parent = 0);
    ~page_1();
    QCheckBox* getLogCheck();
    void addGraphView(QWidget *plot);

    QLabel* getPathLabel();
Q_SIGNALS:
    void min_box_changed(int);
    void max_box_changed(int);
    void click_read(bool);
    void state_changed(int);


public Q_SLOTS:
    void showPath(QString path);
    void setcheckState(Qt::CheckState state);
    void setbtnText(bool onClicked);
    void setEnable(bool enabled);



private:
    Ui::page_1 *ui;



};

#endif // PAGE_1_H
