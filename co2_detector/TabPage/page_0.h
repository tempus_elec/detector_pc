#ifndef PAGE_0_H
#define PAGE_0_H

#include <QWidget>
#include <QLabel>
#include <QComboBox>
#include <QTabWidget>
#include <doublerectview.h>
#include <QPushButton>
#include <QList>
#include <sensor.h>
#include <QTableView>
#include <tableview.h>
#include <doublerectmodel.h>
#include <doublerectview.h>
#include <tablemodel.h>
#include <hbox.h>
#include <QLabel>



namespace Ui {
class page_0;
}

class page_0 : public QWidget
{
    Q_OBJECT

public:
    explicit page_0(QWidget *parent = 0);
    ~page_0();
    QTableView* getTableView();
    void setModel(QList<DoubleRectModel*> list);
    void setModel(TableModel *tmodel);



Q_SIGNALS:
    void min_box_changed(int);
    void max_box_changed(int);

    void click_i2c_scan(bool);
    void click_chksensor(bool);
    void click_read(bool);
    void click_write_id(bool);
    void click_options(bool);
    void click_btn_voltage(bool);
    void text_Changed(QString);

public Q_SLOTS:
    void setADCValue(int i , float val);
    void setADCStatus(int i , QColor color);
    void setRefVoltage(float vol);
    void setEnable(bool enabled);



private:
    Ui::page_0 *ui;

    QPushButton scan;
    QPushButton btn_write_id;
    QPushButton chksensor;
    QPushButton btn_setting;
    QPushButton btn_options;
    TableView *dataView;
    QList<RectView*> rectlist;
    QList<DoubleRectView*> doublerectlist;
    QList<HBox*> boxlist;



  //  QPushButton



};

#endif // PAGE_0_H
