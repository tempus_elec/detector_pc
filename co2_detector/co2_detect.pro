#-------------------------------------------------
#
# Project created by QtCreator 2018-06-07T09:17:35
#
#-------------------------------------------------

QT       += core gui serialport xlsx svg printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = co2_detect
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    Component/sensor.cpp \
    Component/xlsxwriter.cpp \
    Delegate/controller_0.cpp \
    Delegate/controller_1.cpp \
    Model/rectmodel.cpp \
    Model/tablemodel.cpp \
    Model/cellmodel.cpp \
    qwt/curvedata.cpp \
    qwt/plot.cpp \
    qwt/signaldata.cpp \
    View/tableview.cpp \
    View/doublerectview.cpp \
    View/rectview.cpp \
    TabPage/page_0.cpp \
    TabPage/page_1.cpp \
    Thread/ReadWorker.cpp \
    Widget/optiondialog.cpp \
    Widget/settingsdialog.cpp \
    Widget/writeiddialog.cpp \
    Widget/hbox.cpp \
    Component/doublesensor.cpp \
    Model/doublerectmodel.cpp \
    Component/sensorlist.cpp

HEADERS += \
        mainwindow.h \
    constant.h \
    Component/sensor.h \
    Component/xlsxwriter.h \
    Delegate/controller_0.h \
    Delegate/controller_1.h \
    Model/rectmodel.h \
    Model/tablemodel.h \
    Model/cellmodel.h \
    qwt/curvedata.h \
    qwt/plot.h \
    qwt/signaldata.h \
    View/tableview.h \
    View/doublerectview.h \
    View/rectview.h \
    TabPage/page_0.h \
    TabPage/page_1.h \
    Thread/ReadWorker.h \
    Widget/optiondialog.h \
    Widget/settingsdialog.h \
    Widget/writeiddialog.h \
    Widget/hbox.h \
    Component/doublesensor.h \
    Model/doublerectmodel.h \
    Component/sensorlist.h

FORMS += \
        mainwindow.ui \
    TabPage/page_0.ui \
    TabPage/page_1.ui \
    Widget/optiondialog.ui \
    Widget/settingsdialog.ui \
    Widget/writeiddialog.ui

INCLUDEPATH += $$PWD/Component $$PWD/Model $$PWD/View $$PWD/qwt $$PWD/Widget $$PWD/TabPage $$PWD/Thread $$PWD/Delegate

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../Qwt-6.1.3-qt-5.7.0/lib/ -lqwt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../Qwt-6.1.3-qt-5.7.0/lib/ -lqwtd

INCLUDEPATH += $$PWD/../../../../../Qwt-6.1.3-qt-5.7.0/include
DEPENDPATH += $$PWD/../../../../../Qwt-6.1.3-qt-5.7.0/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../Qwt-6.1.3-qt-5.7.0/lib/libqwt.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../Qwt-6.1.3-qt-5.7.0/lib/libqwtd.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../Qwt-6.1.3-qt-5.7.0/lib/qwt.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../Qwt-6.1.3-qt-5.7.0/lib/qwtd.lib

RESOURCES += \
    images/resource.qrc
